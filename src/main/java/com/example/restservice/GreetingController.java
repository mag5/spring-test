package com.example.restservice;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @PostMapping("/human")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = @ExampleObject(value = "{\n" +
            "  \"name\": \"Jonas\",\n" +
            "  \"surname\": \"Jonaitis\"\n" +
            "}", externalValue = "Human.json")))
    public ResponseEntity postHuman(@RequestBody Human human) {
        if(Objects.isNull(human)) {
            return ResponseEntity.badRequest().body("No Human.");
        }
        return ResponseEntity.ok().body(human);
    }
}
